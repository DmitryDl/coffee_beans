package by.shag.litvinov.exception;

public class NotContainsCoffeeException extends IllegalStateException {

    public NotContainsCoffeeException(String message) {
        super(message);
    }
}
