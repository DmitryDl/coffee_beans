package by.shag.litvinov.service;

import by.shag.litvinov.exception.NotContainsCoffeeException;
import by.shag.litvinov.jpa.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SpanishCoffeeShop {

    @Qualifier("spanishEspresso")
    @Autowired
    private Espresso espresso;

    @Qualifier("spanishAmericano")
    @Autowired
    private Americano americano;

    @Qualifier("spanishCappuccino")
    @Autowired
    private Cappuccino cappuccino;

    @Qualifier("spanishLatteMacchiato")
    @Autowired
    private LatteMacchiato latteMacchiato;

    @Qualifier("spanishIrish")
    @Autowired
    private Irish irish;

    @Qualifier("spanishRaf")
    @Autowired
    private Raf raf;

    public Coffee getByType(NameOfCoffee nameOfCoffee) throws NotContainsCoffeeException {
        return switch (nameOfCoffee) {
            case ESPRESSO -> espresso;
            case AMERICANO -> americano;
            case CAPPUCCINO -> cappuccino;
            case LATTE_MACCHIATO -> latteMacchiato;
            case IRISH -> irish;
            case RAF -> raf;
            default -> throw new NotContainsCoffeeException("----------------\n" +
                    "Spanish coffee shop doesn't contains coffee " +
                    nameOfCoffee.getTitle() + ".");
        };
    }
}
