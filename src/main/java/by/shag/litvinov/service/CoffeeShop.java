package by.shag.litvinov.service;

import by.shag.litvinov.jpa.Coffee;
import by.shag.litvinov.jpa.Countries;
import by.shag.litvinov.jpa.NameOfCoffee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoffeeShop {

    @Autowired
    private ItalianCoffeeShop italianCoffeeShop;

    @Autowired
    private SpanishCoffeeShop spanishCoffeeShop;

    @Autowired
    private GermanCoffeeShop germanCoffeeShop;

    public Coffee makeCoffee(NameOfCoffee nameOfCoffee, Countries country) {
        return switch (country) {
            case ITALY -> italianCoffeeShop.getByType(nameOfCoffee);
            case SPAIN -> spanishCoffeeShop.getByType(nameOfCoffee);
            case GERMANY -> germanCoffeeShop.getByType(nameOfCoffee);
        };
    }
}
