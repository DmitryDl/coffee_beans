package by.shag.litvinov.service;

import by.shag.litvinov.exception.NotContainsCoffeeException;
import by.shag.litvinov.jpa.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GermanCoffeeShop {

    @Qualifier("germanEspresso")
    @Autowired
    private Espresso espresso;

    @Qualifier("germanAmericano")
    @Autowired
    private Americano americano;

    @Qualifier("germanCappuccino")
    @Autowired
    private Cappuccino cappuccino;

    @Qualifier("germanLatteMacchiato")
    @Autowired
    private LatteMacchiato latteMacchiato;

    @Qualifier("germanIrish")
    @Autowired
    private Irish irish;

    @Qualifier("germanRaf")
    @Autowired
    private Raf raf;

    public Coffee getByType(NameOfCoffee nameOfCoffee) throws NotContainsCoffeeException {
        return switch (nameOfCoffee) {
            case ESPRESSO -> espresso;
            case AMERICANO -> americano;
            case CAPPUCCINO -> cappuccino;
            case LATTE_MACCHIATO -> latteMacchiato;
            case IRISH -> irish;
            case RAF -> raf;
            default -> throw new NotContainsCoffeeException("----------------\n" +
                    "German coffee shop doesn't contains coffee " +
                    nameOfCoffee.getTitle() + ".");
        };
    }
}
