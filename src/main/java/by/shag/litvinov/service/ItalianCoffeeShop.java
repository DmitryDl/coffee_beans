package by.shag.litvinov.service;


import by.shag.litvinov.exception.NotContainsCoffeeException;
import by.shag.litvinov.jpa.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ItalianCoffeeShop {

    @Qualifier("italianEspresso")
    @Autowired
    private Espresso espresso;

    @Qualifier("italianAmericano")
    @Autowired
    private Americano americano;

    @Qualifier("italianCappuccino")
    @Autowired
    private Cappuccino cappuccino;

    @Qualifier("italianLatteMacchiato")
    @Autowired
    private LatteMacchiato latteMacchiato;

    @Qualifier("italianIrish")
    @Autowired
    private Irish irish;

    @Qualifier("italianRaf")
    @Autowired
    private Raf raf;

    public Coffee getByType(NameOfCoffee nameOfCoffee) throws NotContainsCoffeeException {
        return switch (nameOfCoffee) {
            case ESPRESSO -> espresso;
            case AMERICANO -> americano;
            case CAPPUCCINO -> cappuccino;
            case LATTE_MACCHIATO -> latteMacchiato;
            case IRISH -> irish;
            case RAF -> raf;
            default -> throw new NotContainsCoffeeException("----------------\n" +
                    "Italian coffee shop doesn't contains coffee " +
                    nameOfCoffee.getTitle() + ".");
        };
    }
}
