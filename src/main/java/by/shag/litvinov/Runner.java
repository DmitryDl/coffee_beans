package by.shag.litvinov;

import by.shag.litvinov.exception.NotContainsCoffeeException;
import by.shag.litvinov.jpa.Coffee;
import by.shag.litvinov.jpa.Countries;
import by.shag.litvinov.jpa.NameOfCoffee;
import by.shag.litvinov.service.CoffeeShop;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;

@SpringBootApplication
public class Runner implements ApplicationContextAware {

    private static ApplicationContext context;

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);

        Map<String, Coffee> coffeeMap = context.getBeansOfType(Coffee.class);
        for (Map.Entry<String, Coffee> coffee : coffeeMap.entrySet()) {
            System.out.println("-----------------------------------");
            System.out.println(coffee.getKey() + " - " + coffee.getValue().getReceipt());
        }

        CoffeeShop coffeeShop = context.getBean(CoffeeShop.class);

        System.out.println("******************   Coffee shop   ******************");
        try {
            System.out.println(coffeeShop.makeCoffee(NameOfCoffee.ESPRESSO, Countries.ITALY).getReceipt());
            System.out.println(coffeeShop.makeCoffee(NameOfCoffee.LATTE_MACCHIATO, Countries.GERMANY).getReceipt());
            System.out.println(coffeeShop.makeCoffee(NameOfCoffee.LATTE_MACCHIATO, Countries.SPAIN).getReceipt());
            System.out.println(coffeeShop.makeCoffee(NameOfCoffee.GLACE, Countries.SPAIN).getReceipt()); //такого кофе нет
        } catch (NotContainsCoffeeException e) {
            System.err.println(e.getMessage());
        }

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
