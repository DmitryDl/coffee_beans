package by.shag.litvinov.jpa;

public abstract class Coffee {

    protected NameOfCoffee nameOfCoffee;
    protected Integer ice;
    protected boolean cinnamon;
    protected boolean lemon;

    public void setNameOfCoffee(NameOfCoffee nameOfCoffee) {
        this.nameOfCoffee = nameOfCoffee;
    }

    public void setIce(Integer ice) {
        this.ice = ice;
    }

    public void setCinnamon(boolean cinnamon) {
        this.cinnamon = cinnamon;
    }

    public void setLemon(boolean lemon) {
        this.lemon = lemon;
    }

    public String getReceipt() {
        String receipt = "Coffee " + nameOfCoffee + " contains of ingredients: \n";
        if (ice > 0) {
            receipt = receipt + ice + " ice cube \n";
        }
        if (cinnamon) {
            receipt = receipt + " cinnamon \n";
        }
        if (lemon) {
            receipt = receipt + " lemon \n";
        }
        return receipt;
    }
}
