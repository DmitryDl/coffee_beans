package by.shag.litvinov.jpa;

import org.springframework.beans.factory.annotation.Value;

public class Raf extends Coffee {

    @Value("${raf.arabica.gram}")
    private Integer arabicaGram;

    @Value("${raf.water.gram}")
    private Integer waterGram;

    @Value("${raf.whipped.milk.gram}")
    private Integer whippedMilkGram;

    @Value("${raf.syrup.gram}")
    private Integer syrupGram;

    public Raf() {
        super.nameOfCoffee = NameOfCoffee.RAF;
        super.ice = 0;
        super.cinnamon = false;
        super.lemon = false;
    }

    @Override
    public String getReceipt() {
        String receipt = super.getReceipt();
        receipt = receipt + arabicaGram + " gram of Coffee arabica \n";
        receipt = receipt + waterGram + " gram of water \n";
        receipt = receipt + whippedMilkGram + "gram of whipped milk \n";
        receipt = receipt + syrupGram + "gram of syrup gram \n";
        return receipt;
    }

    @Override
    public String toString() {
        return "Raf{" +
                "nameOfCoffee=" + nameOfCoffee +
                ", arabicaGram=" + arabicaGram +
                ", waterGram=" + waterGram +
                ", whippedMilkGram=" + whippedMilkGram +
                ", syrupGram=" + syrupGram +
                ", ice=" + ice +
                '}';
    }
}
