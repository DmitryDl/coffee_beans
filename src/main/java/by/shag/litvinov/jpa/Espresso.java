package by.shag.litvinov.jpa;

import org.springframework.beans.factory.annotation.Value;

public class Espresso extends Coffee {

    @Value("${espresso.arabica.gram}")
    private Integer arabicaGram;

    @Value("${espresso.water.gram}")
    private Integer waterGram;

    public Espresso() {
        super.nameOfCoffee = NameOfCoffee.ESPRESSO;
        super.ice = 0;
        super.cinnamon = false;
        super.lemon = false;
    }

    @Override
    public String getReceipt() {
        String receipt = super.getReceipt();
        receipt = receipt + arabicaGram + " gram of Coffee arabica \n";
        receipt = receipt + waterGram + " gram of water \n";
        return receipt;
    }

    @Override
    public String toString() {
        return "Espresso{" +
                "nameOfCoffee=" + nameOfCoffee.getTitle() +
                ", arabicaGram=" + arabicaGram +
                ", waterGram=" + waterGram +
                ", ice=" + ice +
                '}';
    }
}
