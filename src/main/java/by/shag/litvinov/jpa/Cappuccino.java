package by.shag.litvinov.jpa;

import org.springframework.beans.factory.annotation.Value;

public class Cappuccino extends Coffee {

    @Value("${cappuccino.arabica.gram}")
    private Integer arabicaGram;

    @Value("${cappuccino.water.gram}")
    private Integer waterGram;

    @Value("${cappuccino.milk.gram}")
    private Integer milkGram;

    @Value("${cappuccino.whipped.milk.gram}")
    private Integer whippedMilkGram;

    public Cappuccino() {
        super.nameOfCoffee = NameOfCoffee.CAPPUCCINO;
        super.ice = 0;
        super.cinnamon = false;
        super.lemon = false;
    }

    @Override
    public String getReceipt() {
        String receipt = super.getReceipt();
        receipt = receipt + arabicaGram + " gram of Coffee arabica \n";
        receipt = receipt + waterGram + " gram of water \n";
        receipt = receipt + milkGram + " gram of milk \n";
        receipt = receipt + whippedMilkGram + " gram of whipped milk \n";
        return receipt;
    }

    @Override
    public String toString() {
        return "Cappuccino{" +
                "nameOfCoffee=" + nameOfCoffee.getTitle() +
                ", arabicaGram=" + arabicaGram +
                ", waterGram=" + waterGram +
                ", milkGram=" + milkGram +
                ", whippedMilkGram=" + whippedMilkGram +
                ", ice=" + ice +
                '}';
    }
}
