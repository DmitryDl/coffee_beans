package by.shag.litvinov.jpa;

import org.springframework.beans.factory.annotation.Value;

public class Americano extends Coffee {

    @Value("${americano.arabica.gram}")
    private Integer arabicaGram;

    @Value("${americano.water.gram}")
    private Integer waterGram;

    public Americano() {
        super.nameOfCoffee = NameOfCoffee.AMERICANO;
        super.ice = 0;
        super.cinnamon = false;
        super.lemon = false;

    }

    @Override
    public String getReceipt() {
        String receipt = super.getReceipt();
        receipt = receipt + arabicaGram + " gram of Coffee arabica \n";
        receipt = receipt + waterGram + " gram of water \n";
        return receipt;
    }

    @Override
    public String toString() {
        return "Americano{" +
                "nameOfCoffee=" + nameOfCoffee.getTitle() +
                ", arabicaGram=" + arabicaGram +
                ", waterGram=" + waterGram +
                ", ice=" + ice +
                '}';
    }
}
