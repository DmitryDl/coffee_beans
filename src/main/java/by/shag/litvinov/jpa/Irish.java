package by.shag.litvinov.jpa;

import org.springframework.beans.factory.annotation.Value;

public class Irish extends Coffee {

    @Value("${irish.arabica.gram}")
    private Integer arabicaGram;

    @Value("${irish.water.gram}")
    private Integer waterGram;

    @Value("${irish.whipped.cream.gram}")
    private Integer whippedCreamGram;

    @Value("${irish.whiskey.gram}")
    private Integer whiskeyGram;

    public Irish() {
        super.nameOfCoffee = NameOfCoffee.IRISH;
        super.ice = 0;
        super.cinnamon = false;
        super.lemon = false;
    }

    @Override
    public String getReceipt() {
        String receipt = super.getReceipt();
        receipt = receipt + arabicaGram + " gram of Coffee arabica \n";
        receipt = receipt + waterGram + " gram of water \n";
        receipt = receipt + whippedCreamGram + "gram of whipped cream \n";
        receipt = receipt + whiskeyGram + "gram of whiskey \n";
        return receipt;
    }

    @Override
    public String toString() {
        return "Irish{" +
                "nameOfCoffee=" + nameOfCoffee +
                ", arabicaGram=" + arabicaGram +
                ", waterGram=" + waterGram +
                ", whippedCreamGram=" + whippedCreamGram +
                ", whiskeyGram=" + whiskeyGram +
                ", ice=" + ice +
                '}';
    }
}
