package by.shag.litvinov.jpa;

public enum NameOfCoffee {

    ESPRESSO("Espresso"),
    AMERICANO("Americano"),
    CAPPUCCINO("Cappuccino"),
    LATTE_MACCHIATO("Latte macchiato"),
    IRISH("Irish"),
    RAF("Raf"),
    GLACE("Glace");

    private String title;

    NameOfCoffee(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
