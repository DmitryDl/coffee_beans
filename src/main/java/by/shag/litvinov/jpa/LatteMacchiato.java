package by.shag.litvinov.jpa;

import org.springframework.beans.factory.annotation.Value;

public class LatteMacchiato extends Coffee {

    @Value("${latte_macchiato.arabica.gram}")
    private Integer arabicaGram;

    @Value("${latte_macchiato.water.gram}")
    private Integer waterGram;

    @Value("${latte_macchiato.milk.gram}")
    private Integer milkGram;

    @Value("${latte_macchiato.whipped.cream.gram}")
    private Integer whippedCreamGram;

    public LatteMacchiato() {
        super.nameOfCoffee = NameOfCoffee.LATTE_MACCHIATO;
        super.ice = 0;
        super.cinnamon = false;
        super.lemon = false;
    }

    @Override
    public String getReceipt() {
        String receipt = super.getReceipt();
        receipt = receipt + arabicaGram + " gram of Coffee arabica \n";
        receipt = receipt + waterGram + " gram of water \n";
        receipt = receipt + milkGram + " gram of milk \n";
        receipt = receipt + whippedCreamGram + " gram of whipped cream \n";
        return receipt;
    }

    @Override
    public String toString() {
        return "LatteMacchiato{" +
                "nameOfCoffee=" + nameOfCoffee +
                ", arabicaGram=" + arabicaGram +
                ", waterGram=" + waterGram +
                ", milkGram=" + milkGram +
                ", whippedCreamGram=" + whippedCreamGram +
                ", ice=" + ice +
                '}';
    }
}
