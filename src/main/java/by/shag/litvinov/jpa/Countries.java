package by.shag.litvinov.jpa;

public enum Countries {

    ITALY("Italy"),
    SPAIN("Spain"),
    GERMANY("Germany");

    private String title;

    Countries(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return title;
    }
}
