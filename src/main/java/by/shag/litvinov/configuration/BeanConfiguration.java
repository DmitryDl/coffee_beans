package by.shag.litvinov.configuration;

import by.shag.litvinov.jpa.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public Espresso italianEspresso() {
        Espresso espresso = new Espresso();
        espresso.setLemon(true);
        return espresso;
    }

    @Bean
    public Espresso spanishEspresso() {
        Espresso espresso = new Espresso();
        espresso.setIce(1);
        return espresso;
    }

    @Bean
    public Espresso germanEspresso() {
        Espresso espresso = new Espresso();
        espresso.setCinnamon(true);
        return espresso;
    }

    @Bean
    public Americano italianAmericano() {
        Americano americano = new Americano();
        americano.setLemon(true);
        return americano;
    }

    @Bean
    public Americano spanishAmericano() {
        Americano americano = new Americano();
        americano.setIce(2);
        return americano;
    }

    @Bean
    public Americano germanAmericano() {
        Americano americano = new Americano();
        americano.setCinnamon(true);
        return americano;
    }

    @Bean
    public Cappuccino italianCappuccino() {
        Cappuccino cappuccino = new Cappuccino();
        cappuccino.setLemon(true);
        return cappuccino;
    }

    @Bean
    public Cappuccino spanishCappuccino() {
        Cappuccino cappuccino = new Cappuccino();
        cappuccino.setIce(3);
        return cappuccino;
    }

    @Bean
    public Cappuccino germanCappuccino() {
        Cappuccino cappuccino = new Cappuccino();
        cappuccino.setCinnamon(true);
        return cappuccino;
    }

    @Bean
    public LatteMacchiato italianLatteMacchiato() {
        LatteMacchiato latteMacchiato = new LatteMacchiato();
        latteMacchiato.setLemon(true);
        return latteMacchiato;
    }

    @Bean
    public LatteMacchiato spanishLatteMacchiato() {
        LatteMacchiato latteMacchiato = new LatteMacchiato();
        latteMacchiato.setIce(4);
        return latteMacchiato;
    }

    @Bean
    public LatteMacchiato germanLatteMacchiato() {
        LatteMacchiato latteMacchiato = new LatteMacchiato();
        latteMacchiato.setCinnamon(true);
        return latteMacchiato;
    }

    @Bean
    public Irish italianIrish() {
        Irish irish = new Irish();
        irish.setLemon(true);
        return irish;
    }

    @Bean
    public Irish spanishIrish() {
        Irish irish = new Irish();
        irish.setIce(5);
        return irish;
    }

    @Bean
    public Irish germanIrish() {
        Irish irish = new Irish();
        irish.setCinnamon(true);
        return irish;
    }

    @Bean
    public Raf italianRaf() {
        Raf raf = new Raf();
        raf.setLemon(true);
        return raf;
    }

    @Bean
    public Raf spanishRaf() {
        Raf raf = new Raf();
        raf.setIce(6);
        return raf;
    }

    @Bean
    public Raf germanRaf() {
        Raf raf = new Raf();
        raf.setCinnamon(true);
        return raf;
    }
}
